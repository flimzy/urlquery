package urlquery

import (
	"net/url"
	"testing"

	"gitlab.com/flimzy/testy"
)

func Test_jsonapi(t *testing.T) {
	type tt struct {
		input interface{}
		want  string
		err   string
	}

	tests := testy.NewTable()
	tests.Add("nil input", tt{
		input: nil,
		want:  "",
	})
	tests.Add("empty input", tt{
		input: struct{}{},
		want:  "",
	})
	tests.Add("*string", tt{
		input: struct{ Foo *string }{Foo: &[]string{"bar"}[0]},
		want:  "Foo=bar",
	})
	tests.Add("string", tt{
		input: struct{ Foo string }{Foo: "bar"},
		want:  "Foo=bar",
	})
	tests.Add("unsupported type: chan", tt{
		input: struct{ Foo chan bool }{},
		err:   "type chan not permitted as query value",
	})
	tests.Add("unsupported type: func", tt{
		input: struct{ Foo func() }{},
		err:   "type func not permitted as query value",
	})
	tests.Add("int", tt{
		input: struct{ Foo int }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("int8", tt{
		input: struct{ Foo int8 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("int16", tt{
		input: struct{ Foo int16 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("int32", tt{
		input: struct{ Foo int32 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("int64", tt{
		input: struct{ Foo int64 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("uint", tt{
		input: struct{ Foo uint }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("uint8", tt{
		input: struct{ Foo uint8 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("uint16", tt{
		input: struct{ Foo uint16 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("uint32", tt{
		input: struct{ Foo uint32 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("uint64", tt{
		input: struct{ Foo uint64 }{Foo: 123},
		want:  "Foo=123",
	})
	tests.Add("float32", tt{
		input: struct{ Foo float32 }{Foo: 1.23},
		want:  "Foo=1.23",
	})
	tests.Add("float64", tt{
		input: struct{ Foo float64 }{Foo: 1.23},
		want:  "Foo=1.23",
	})
	tests.Add("complex64", tt{
		input: struct{ Foo complex64 }{},
		want:  "Foo=(0+0i)",
	})
	tests.Add("complex128", tt{
		input: struct{ Foo complex128 }{},
		want:  "Foo=(0+0i)",
	})
	tests.Add("slice", tt{
		input: map[string][]string{"foo": {"one", "two"}},
		want:  "foo[]=one&foo[]=two",
	})
	tests.Add("slice with mixed types", tt{
		input: map[string][]interface{}{"foo": {"one", map[string]string{"two": "three"}}},
		want:  "foo[]=one&foo[][two]=three",
	})
	tests.Add("transistor.fm test 1", tt{
		input: struct {
			Include []string `url:"include"`
		}{
			Include: []string{"show"},
		},
		want: "include[]=show",
	})
	tests.Add("transistor.fm test 2", tt{
		input: struct {
			Fields map[string][]string `url:"fields"`
		}{
			Fields: map[string][]string{
				"show": {"title"},
			},
		},
		want: "fields[show][]=title",
	})
	tests.Add("pagination", func() interface{} {
		type Pagination struct {
			Page int `url:"page"`
			Per  int `url:"per"`
		}
		return tt{
			input: struct {
				Pagination Pagination `url:"pagination"`
			}{
				Pagination: Pagination{
					Page: 1,
					Per:  5,
				},
			},
			want: "pagination[page]=1&pagination[per]=5",
		}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		e := JSONAPI()
		values, err := e.Encode(tt.input)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}

		if got, _ := url.QueryUnescape(values.Encode()); got != tt.want {
			t.Errorf("Unexpected result: %s", got)
		}
	})
}
