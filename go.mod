module gitlab.com/flimzy/urlquery

go 1.17

require (
	github.com/google/go-cmp v0.5.9
	gitlab.com/flimzy/testy v0.12.0
)

require (
	github.com/Masterminds/semver/v3 v3.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/otiai10/copy v1.7.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
