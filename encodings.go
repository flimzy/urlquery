package urlquery

import (
	"encoding"
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"
)

/*
foo[bar][]		// map[string][]string
foo[bar]		// map[string]
foo[bar][0]		// map[string]string
foo.bar			// map[string]string
foo.bar[0]		// map[string][]string
foo[bar][baz]   // map[string]map[string]string
*/

// The following options control array expression when encoding.
const (
	ArrayImplicit = iota
	ArrayExplicit
	ArrayIndexed
	ArrayCSV
	ArrayNone
)

// The following options control sub-key expression when encoding.
const (
	KeysIndexed = iota
	KeysDotted
	KeysNone
)

const (
	defaultStructTag = "url"
)

// Encoding represents a specific set of encoding/decoding rules for URL
// query mapping.
type Encoding struct {
	// FormatBool specifies a custom Bool formatting.  Defaults to calling
	// strconv.FormatBool, which returns the strings "true" and "false".
	FormatBool func(bool) string
	// ArrayMode indicates how to encode arrays. The options are:
	//
	//	- ArrayImplicit -- No special treatment (default)
	//	- ArrayExplicit -- Adds a [] suffix to the key name
	//	- ArrayIndexed  -- Each array element is indexed by adding a [n] suffix to the key name
	//	- ArrayCSV      -- All values are added to a single key, as a list of CSV values
	//	- ArrayNone     -- An array will return an error
	ArrayMode int
	// KeyMode indicates how to encode object keys. The options are:
	//
	//	- KeysIndexed -- Keys are wrapped in square brackets
	//	- KeysDotted  -- Keys are separated by periods
	//	- KeysNone    -- Any sub-keys will return an error.
	KeyMode int
	// Tag specifies the name of the struct tag to look for. Defaults to url.
	// Set Tag to "-" to disable struct tag parsing.
	Tag string
}

// JSONAPI returns an Encoding that produces query parameters compatible with
// JSON:API requests.
func JSONAPI() *Encoding {
	return &Encoding{
		ArrayMode: ArrayExplicit,
		KeyMode:   KeysIndexed,
	}
}

// Primitive is one of the following underlying types:
//
//   - string
//   - []Primitive
//   - map[string]Primitive
type Primitive interface{}

// Encode converts i to a set of url.Values.
func (e *Encoding) Encode(i interface{}) (url.Values, error) {
	if i == nil {
		return nil, nil
	}
	ast, err := e.toAST(i)
	if err != nil {
		return nil, err
	}
	return e.ast2query(ast)
}

func (e *Encoding) toAST(i interface{}) (map[string]Primitive, error) {
	if i == nil {
		return nil, nil
	}
	primitive, err := e.toPrimitive(reflect.ValueOf(i), tagOpts{})
	if err != nil || primitive == nil {
		return nil, err
	}
	ast, ok := primitive.(map[string]Primitive)
	if !ok {
		return nil, fmt.Errorf("type %T not permitted as top-level object", i)
	}
	return ast, nil
}

func (e *Encoding) ast2query(ast map[string]Primitive) (url.Values, error) {
	if ast == nil {
		return nil, nil
	}
	query := url.Values{}
	for k, v := range ast {
		if err := e.queryAdd(k, v, &query); err != nil {
			return nil, err
		}
	}
	return query, nil
}

func (e *Encoding) queryAdd(prefix string, p Primitive, query *url.Values) error {
	switch t := p.(type) {
	case string:
		query.Add(prefix, t)
		return nil
	case []Primitive:
		if e.ArrayMode == ArrayCSV {
			str := make([]string, len(t))
			for i, p := range t {
				val, ok := p.(string)
				if !ok {
					return fmt.Errorf("%s: slice of mixed types not supported in CSV mode", prefix)
				}
				str[i] = val
			}
			query.Add(prefix, strings.Join(str, ","))
			return nil
		}
		for i, v := range t {
			key, err := e.index(prefix, i)
			if err != nil {
				return err
			}
			if err := e.queryAdd(key, v, query); err != nil {
				return err
			}
		}
		return nil
	case map[string]Primitive:
		keys := make([]string, 0, len(t))
		for k := range t {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		for _, k := range keys {
			v := t[k]
			key, err := e.key(prefix, k)
			if err != nil {
				return err
			}
			if err := e.queryAdd(key, v, query); err != nil {
				return err
			}
		}
		return nil
	}
	panic("bug: unexpected type passed to queryAdd as Primitive")
}

func (e *Encoding) index(prefix string, i int) (string, error) {
	switch e.ArrayMode {
	case ArrayImplicit:
		return prefix, nil
	case ArrayExplicit:
		return prefix + "[]", nil
	case ArrayIndexed:
		return fmt.Sprintf("prefix[%d]", i), nil
	case ArrayCSV:
		panic("bug: don't use e.index() for CSV array encoding")
	case ArrayNone:
		return "", errors.New("arrays not permitted by encoding")
	}
	return "", fmt.Errorf("unrecognized ArrayMode: %v", e.ArrayMode)
}

func (e *Encoding) key(prefix, k string) (string, error) {
	switch e.KeyMode {
	case KeysIndexed:
		return prefix + "[" + k + "]", nil
	case KeysDotted:
		return prefix + "." + k, nil
	case KeysNone:
		return "", errors.New("maps not permitted by encoding")
	}
	return "", fmt.Errorf("unrecognized KeyMode: %v", e.KeyMode)
}

func (e *Encoding) toPrimitive(v reflect.Value, opts tagOpts) (Primitive, error) {
	for v.Kind() == reflect.Ptr || v.Kind() == reflect.Interface {
		v = v.Elem()
	}
	switch v.Kind() {
	case reflect.Invalid:
		return nil, nil
	case reflect.Chan, reflect.Func:
		return nil, fmt.Errorf("type %s not permitted as query value", v.Kind())
	case reflect.Slice, reflect.Array:
		result := make([]Primitive, v.Len())
		for i := 0; i < v.Len(); i++ {
			val, err := e.toPrimitive(v.Index(i), tagOpts{})
			if err != nil {
				return nil, err
			}
			result[i] = val
		}
		return result, nil
	case reflect.Map:
		k := v.MapKeys()
		result := make(map[string]Primitive, len(k))

		for iter := v.MapRange(); iter.Next(); {
			k := iter.Key()
			key, err := e.toPrimitive(k, tagOpts{})
			if err != nil {
				return nil, fmt.Errorf("type %s not permitted as map key", k.Kind())
			}
			strKey, ok := key.(string)
			if !ok {
				return nil, fmt.Errorf("type %s not permitted as map key", k.Kind())
			}
			val, err := e.toPrimitive(iter.Value(), tagOpts{})
			if err != nil {
				return nil, err
			}
			result[strKey] = val
		}
		return result, nil
	}
	iv := v.Interface()
	switch t := iv.(type) {
	case time.Time:
		format := opts.format
		if format == "" {
			format = time.RFC3339Nano
		}
		return t.Format(format), nil
	case fmt.Stringer:
		return t.String(), nil
	case encoding.TextMarshaler:
		byt, err := t.MarshalText()
		return string(byt), err
	case string:
		return t, nil
	case int:
		return strconv.FormatInt(int64(t), 10), nil
	case int8:
		return strconv.FormatInt(int64(t), 10), nil
	case int16:
		return strconv.FormatInt(int64(t), 10), nil
	case int32:
		return strconv.FormatInt(int64(t), 10), nil
	case int64:
		return strconv.FormatInt(t, 10), nil
	case uint:
		return strconv.FormatUint(uint64(t), 10), nil
	case uint8:
		return strconv.FormatUint(uint64(t), 10), nil
	case uint16:
		return strconv.FormatUint(uint64(t), 10), nil
	case uint32:
		return strconv.FormatUint(uint64(t), 10), nil
	case uint64:
		return strconv.FormatUint(t, 10), nil
	case float32:
		return strconv.FormatFloat(float64(t), 'f', -1, 32), nil
	case float64:
		return strconv.FormatFloat(t, 'f', -1, 64), nil
	case complex64:
		return strconv.FormatComplex(complex128(t), 'f', -1, 64), nil
	case complex128:
		return strconv.FormatComplex(t, 'f', -1, 128), nil
	case bool:
		if e.FormatBool != nil {
			return e.FormatBool(t), nil
		}
		return strconv.FormatBool(t), nil
	}

	if v.Kind() == reflect.Struct {
		return e.struct2Primitive(v)
	}

	return nil, fmt.Errorf("type %T not permitted as query value", iv)
}

func (e *Encoding) struct2Primitive(v reflect.Value) (map[string]Primitive, error) {
	tag := e.Tag
	if tag == "" {
		tag = defaultStructTag
	}
	t := v.Type()
	result := make(map[string]Primitive, v.NumField())
	for i := 0; i < v.NumField(); i++ {
		f := t.Field(i)
		name, opts := fieldName(&f, tag)
		if name == "-" {
			continue
		}
		fv := v.Field(i)
		if opts.omitempty && isEmpty(fv) {
			continue
		}

		if f.Anonymous {
			subResult, err := e.toPrimitive(fv, opts)
			if err != nil {
				return nil, err
			}
			if t, ok := subResult.(map[string]Primitive); ok {
				for k, v := range t {
					if _, found := result[k]; !found {
						result[k] = v
					}
				}
				continue
			}
			result[name] = subResult
			continue
		}
		val, err := e.toPrimitive(v.Field(i), opts)
		if err != nil {
			return nil, err
		}
		result[name] = val
	}
	return result, nil
}

type tagOpts struct {
	omitempty bool
	format    string
}

func fieldName(f *reflect.StructField, tagName string) (string, tagOpts) {
	opts := tagOpts{}
	if tagName == "-" {
		return f.Name, opts
	}
	tag := f.Tag.Get(tagName)
	if tag == "" {
		return f.Name, opts
	}
	parts := strings.Split(tag, ",")
	name := parts[0]
	if name == "" {
		name = f.Name
	}
	for _, part := range parts[1:] {
		if part == "omitempty" {
			opts.omitempty = true
			continue
		}
		if strings.HasPrefix(part, "format=") {
			opts.format = strings.TrimPrefix(part, "format=")
		}
	}
	return name, opts
}

// URLMarshaler may be implemented by any type to support conversion to query
// parameters.
type URLMarshaler interface {
	MarshalURL() (Primitive, error)
}

// Emptier may be used in conjunction with the omitempty struct tag. Any type
// that implements this interface may return false here to be omitted in output
// when the tag is present.
type Emptier interface {
	IsEmpty() bool
}

func isEmpty(v reflect.Value) bool {
	if emptier, ok := v.Interface().(Emptier); ok {
		return emptier.IsEmpty()
	}
	return v.IsZero()
}
