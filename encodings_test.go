package urlquery

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/testy"
)

type stringer struct{}

func (stringer) String() string {
	return "Stringer!"
}

type textMarshaler struct{}

func (textMarshaler) MarshalText() ([]byte, error) {
	return []byte("Text Marshaler!"), nil
}

func Test_toAST(t *testing.T) {
	t.Parallel()
	type tt struct {
		encoding *Encoding
		input    interface{}
		want     map[string]Primitive
		err      string
	}

	tests := testy.NewTable()
	tests.Add("nil input", tt{
		input: nil,
		want:  nil,
	})
	tests.Add("nil struct", func() interface{} {
		type Struct struct {
			Foo string
		}
		var x *Struct
		return tt{
			input: x,
			want:  nil,
		}
	})
	tests.Add("nil map", func() interface{} {
		type X map[string]string
		var x *X
		return tt{
			input: x,
			want:  nil,
		}
	})
	tests.Add("empty input", tt{
		input: struct{}{},
		want:  map[string]Primitive{},
	})
	tests.Add("*string", tt{
		input: struct{ Foo *string }{Foo: &[]string{"bar"}[0]},
		want:  map[string]Primitive{"Foo": "bar"},
	})
	tests.Add("string", tt{
		input: struct{ Foo string }{Foo: "bar"},
		want:  map[string]Primitive{"Foo": "bar"},
	})
	tests.Add("unsupported type: chan", tt{
		input: struct{ Foo chan bool }{},
		err:   "type chan not permitted as query value",
	})
	tests.Add("unsupported type: func", tt{
		input: struct{ Foo func() }{},
		err:   "type func not permitted as query value",
	})
	tests.Add("int", tt{
		input: struct{ Foo int }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("int8", tt{
		input: struct{ Foo int8 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("int16", tt{
		input: struct{ Foo int16 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("int32", tt{
		input: struct{ Foo int32 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("int64", tt{
		input: struct{ Foo int64 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("uint", tt{
		input: struct{ Foo uint }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("uint8", tt{
		input: struct{ Foo uint8 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("uint16", tt{
		input: struct{ Foo uint16 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("uint32", tt{
		input: struct{ Foo uint32 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("uint64", tt{
		input: struct{ Foo uint64 }{Foo: 123},
		want:  map[string]Primitive{"Foo": "123"},
	})
	tests.Add("float32", tt{
		input: struct{ Foo float32 }{Foo: 1.23},
		want:  map[string]Primitive{"Foo": "1.23"},
	})
	tests.Add("float64", tt{
		input: struct{ Foo float64 }{Foo: 1.23},
		want:  map[string]Primitive{"Foo": "1.23"},
	})
	tests.Add("complex64", tt{
		input: struct{ Foo complex64 }{},
		want:  map[string]Primitive{"Foo": "(0+0i)"},
	})
	tests.Add("complex128", tt{
		input: struct{ Foo complex128 }{},
		want:  map[string]Primitive{"Foo": "(0+0i)"},
	})
	tests.Add("bool", tt{
		input: struct{ Foo bool }{Foo: true},
		want:  map[string]Primitive{"Foo": "true"},
	})
	tests.Add("custom bool", tt{
		encoding: &Encoding{
			FormatBool: func(b bool) string {
				if b {
					return "1"
				}
				return "0"
			},
		},
		input: struct{ Foo bool }{Foo: true},
		want:  map[string]Primitive{"Foo": "1"},
	})
	tests.Add("string slice", tt{
		input: struct{ Foo []string }{Foo: []string{"one", "two", "three"}},
		want:  map[string]Primitive{"Foo": []Primitive{"one", "two", "three"}},
	})
	tests.Add("int slice", tt{
		input: struct{ Foo []int }{Foo: []int{1, 2, 3}},
		want:  map[string]Primitive{"Foo": []Primitive{"1", "2", "3"}},
	})
	tests.Add("chan in slice", tt{
		input: struct{ Foo []interface{} }{Foo: []interface{}{"foo", 3, make(chan bool)}},
		err:   "type chan not permitted as query value",
	})
	tests.Add("int array", tt{
		input: struct{ Foo [3]int }{Foo: [3]int{1, 2, 3}},
		want:  map[string]Primitive{"Foo": []Primitive{"1", "2", "3"}},
	})
	tests.Add("fmt.Stringer", tt{
		input: struct{ Foo interface{} }{Foo: &stringer{}},
		want:  map[string]Primitive{"Foo": "Stringer!"},
	})
	tests.Add("encoding.TextMarshaler", tt{
		input: struct{ Foo interface{} }{Foo: &textMarshaler{}},
		want:  map[string]Primitive{"Foo": "Text Marshaler!"},
	})
	tests.Add("invalid map key", tt{
		input: struct{ Foo interface{} }{Foo: map[[1]int]string{{1}: "foo"}},
		err:   "type array not permitted as map key",
	})
	tests.Add("map[string]string", tt{
		input: struct{ Foo interface{} }{Foo: map[string]string{"foo": "bar"}},
		want:  map[string]Primitive{"Foo": map[string]Primitive{"foo": "bar"}},
	})
	tests.Add("arbitrary map", tt{
		input: struct {
			Foo interface{}
		}{Foo: map[string]interface{}{
			"string": "bar",
			"int":    123,
			"slice":  []string{"one", "two"},
		}},
		want: map[string]Primitive{"Foo": map[string]Primitive{
			"string": "bar",
			"int":    "123",
			"slice":  []Primitive{"one", "two"},
		}},
	})
	tests.Add("child struct", func() interface{} {
		type Bar struct {
			Bar string
		}
		type Foo struct {
			Foo Bar
		}
		return tt{
			input: Foo{Foo: Bar{Bar: "xyzzy"}},
			want:  map[string]Primitive{"Foo": map[string]Primitive{"Bar": "xyzzy"}},
		}
	})
	tests.Add("top-level map", tt{
		input: map[string]string{"foo": "bar"},
		want:  map[string]Primitive{"foo": "bar"},
	})
	tests.Add("default struct tag", tt{
		input: struct {
			Foo string `url:"foo"`
		}{
			Foo: "bar",
		},
		want: map[string]Primitive{"foo": "bar"},
	})
	tests.Add("embedded struct", func() interface{} {
		type One struct {
			One string `url:"one"`
		}
		type Two struct {
			One
			Two string `url:"two"`
		}
		return tt{
			input: Two{
				One: One{One: "1"},
				Two: "2",
			},
			want: map[string]Primitive{"one": "1", "two": "2"},
		}
	})
	tests.Add("embedded interface", tt{
		input: struct {
			fmt.Stringer
			Two    string
			String string
		}{
			Stringer: &stringer{},
			Two:      "two",
		},
		want: map[string]Primitive{"Two": "two", "Stringer": "Stringer!", "String": ""},
	})
	tests.Add("embedded slice", func() interface{} {
		type Foo []string

		return tt{
			input: struct {
				Foo
				Two string
			}{
				Foo: []string{"one", "two"},
				Two: "two",
			},
			want: map[string]Primitive{"Two": "two", "Foo": []Primitive{"one", "two"}},
		}
	})
	tests.Add("custom struct tag", tt{
		encoding: &Encoding{
			Tag: "oink",
		},
		input: struct {
			Foo string `oink:"foo"`
		}{
			Foo: "bar",
		},
		want: map[string]Primitive{"foo": "bar"},
	})
	tests.Add("omit struct tag", tt{
		input: struct {
			Foo string `url:"-"`
			Bar string
		}{
			Foo: "bar",
			Bar: "baz",
		},
		want: map[string]Primitive{"Bar": "baz"},
	})
	tests.Add("disabled struct tag", tt{
		encoding: &Encoding{
			Tag: "-",
		},
		input: struct {
			Foo string `oink:"foo"`
		}{
			Foo: "bar",
		},
		want: map[string]Primitive{"Foo": "bar"},
	})
	tests.Add("omitempty struct tag", tt{
		input: struct {
			Foo string `url:"foo,omitempty"`
			Bar string `url:"bar"`
		}{
			Foo: "",
			Bar: "bar",
		},
		want: map[string]Primitive{"bar": "bar"},
	})
	tests.Add("omitempty struct tag with pointer", tt{
		input: struct {
			Foo *string `url:"foo,omitempty"`
			Bar string  `url:"bar"`
		}{
			Bar: "bar",
		},
		want: map[string]Primitive{"bar": "bar"},
	})

	ts, _ := time.Parse(time.RFC3339, "2010-01-01T01:01:01Z")
	tests.Add("time defaults", tt{
		input: struct {
			Time time.Time
		}{
			Time: ts,
		},
		want: map[string]Primitive{"Time": "2010-01-01T01:01:01Z"},
	})
	tests.Add("time custom format", tt{
		input: struct {
			Time time.Time `url:",format=02 Jan 06"`
		}{
			Time: ts,
		},
		want: map[string]Primitive{"Time": "01 Jan 10"},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		e := tt.encoding
		if e == nil {
			e = &Encoding{}
		}
		got, err := e.toAST(tt.input)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}
